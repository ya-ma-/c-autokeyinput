using System;
using System.Windows.Forms;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.IO;

namespace BackgroundWorkerExample
{
	public class Program
	{

		//DllImport属性宣言？
		[DllImport("user32.dll", SetLastError = true)]
		static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

		[DllImport("user32.dll")]
		[return: MarshalAs(UnmanagedType.Bool)]
		private static extern bool SetForegroundWindow(IntPtr hWnd);

		[DllImport("user32.dll")]
		public static extern IntPtr GetForegroundWindow();


		//Directoryのファイルサイズを取得
		public static long GetDirectorySize(DirectoryInfo dirInfo)
		{
			long size = 0;

			foreach (FileInfo fi in dirInfo.GetFiles())
				size += fi.Length;

			foreach (DirectoryInfo di in dirInfo.GetDirectories())
				size += GetDirectorySize(di);

			return size;
		}


		//After Effect へのキー操作
		static void autokey(string name, Process hp, string dirpath)
		{

			IntPtr hWnd0 = IntPtr.Zero;
			IntPtr hWnd1 = IntPtr.Zero;
			IntPtr hWnd2 = IntPtr.Zero;
			IntPtr hWnd3 = IntPtr.Zero;
			IntPtr hWnd4 = IntPtr.Zero;

			//int h1 = 0;
			//int h2 = 0;
			//int h3 = 0;
			//int w = 0;
			//int froad = 0;
			//Console.Write(name + "\n");
			//Console.Write("はじめ\n");

			System.Threading.Thread.Sleep(1000);

			hWnd0 = FindWindow(null, "Adobe After effects - " + name + "*");

			while (hWnd0 == IntPtr.Zero)
			{
				System.Threading.Thread.Sleep(200);
				hWnd0 = FindWindow(null, "Adobe After effects - " + name);
				if (hWnd0 == IntPtr.Zero)
				{
					hWnd0 = FindWindow(null, "Adobe After effects - " + name + "*");
					System.Threading.Thread.Sleep(200);
					SendKeys.SendWait("^s");
				}
			}

			System.Threading.Thread.Sleep(500);

			while (hWnd1 == IntPtr.Zero)
			{
				System.Threading.Thread.Sleep(200);
				hWnd1 = FindWindow(null, "Adobe After effects - " + name);
				//h1++;
			}

			SetForegroundWindow(hWnd1);
			System.Threading.Thread.Sleep(300);
			SendKeys.SendWait("%(f)");
			System.Threading.Thread.Sleep(300);
			SendKeys.SendWait("{up 12}");

			System.Threading.Thread.Sleep(500);

			SendKeys.SendWait("{ENTER}");

			System.Threading.Thread.Sleep(500);

			while (hWnd2 == IntPtr.Zero)
			{
				System.Threading.Thread.Sleep(200);
				hWnd2 = FindWindow(null, "ファイルを収集");
				//h2++;
			}

			System.Threading.Thread.Sleep(300);

			SetForegroundWindow(hWnd2);

			int u = 0;
			System.Threading.Thread.Sleep(500);

			while (u == 0)
			{
				SendKeys.SendWait("{ENTER}");
				System.Threading.Thread.Sleep(500);
				hWnd3 = FindWindow(null, "フォルダーにファイルを収集");
				if (hWnd3 != IntPtr.Zero)
				{
					u++;
				}
			}

			System.Threading.Thread.Sleep(300);
			SendKeys.SendWait("{RIGHT}");
			System.Threading.Thread.Sleep(300);
			SendKeys.SendWait("{bs 5}");
			System.Threading.Thread.Sleep(500);

			while (SetForegroundWindow(hWnd3))
			{
				SendKeys.SendWait("{ENTER}");
			}

			System.Threading.Thread.Sleep(1000);

			string[] fname = name.Split('.');
			System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(dirpath + "\\" + fname[0]);
			long dirsize = GetDirectorySize(di);
			System.Threading.Thread.Sleep(1000);
			long dirsize2 = GetDirectorySize(di);

			while (dirsize != dirsize2)
			{
				dirsize = GetDirectorySize(di);
				System.Threading.Thread.Sleep(1000);
				dirsize2 = GetDirectorySize(di);
				//froad++;
			}

			hWnd4 = FindWindow(null, "プロジェクトを開く");

			if (hWnd4 != IntPtr.Zero)
			{
				while (SetForegroundWindow(hWnd4))
				{
					System.Threading.Thread.Sleep(1000);
				}
			}

			//Console.Write(h1 + "\n");
			//Console.Write(h2 + "\n");
			//Console.Write(h3 + "\n");
			//Console.Write(froad + "\n");
			System.Threading.Thread.Sleep(3000);
		}

		/// <summary>
		///Main 
		/// </summary>

		static void Main()
		{
			string dirPath = "C:\\Users\\yamamotom\\Desktop\\syuusyuu";
			string[] files = Directory.GetFiles(dirPath);
			for (int i = 0; i < files.Length; i++)
			{
				string path = files[i];
				string[] pArrayData = path.Split('\\');
				int pArrayDataL = pArrayData.Length;
				string basename = pArrayData[pArrayDataL - 1];

				ProcessStartInfo startInfo = new ProcessStartInfo("C:\\Program Files\\Adobe\\Adobe After Effects CS5\\Support Files\\AfterFX.exe");
				startInfo.Arguments = (path);
				startInfo.UseShellExecute = true;
				//startInfo.WindowStyle = ProcessWindowStyle.Hidden;

				Process hProcess = Process.Start(startInfo);

				System.Threading.Thread.Sleep(2000);

				autokey(basename, hProcess, dirPath);
			}
		}
	}
}